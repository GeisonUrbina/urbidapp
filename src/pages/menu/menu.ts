import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';


@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  userName:any;
  userData:any;
  buscarProducto:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage,
    public providerGeneral: GeneralQueryProvider) {
      this.storage.get('user_name').then((val)=>{this.userName = val;});
    
  }
  
  category_page(){
    this.navCtrl.push("CategoryPage");
  }

  shopping_cart(){
    this.navCtrl.push("CartPage");
  }

  favorite_page(){
    this.navCtrl.push("FavoritePage");
  }

  order_page(){
    this.navCtrl.push("OrderPage");
  }

  discount_page(){

  }

  projects_page(){
    this.navCtrl.push("CommunityPage");
  }

  notification_page(){
    this.navCtrl.push("NotificationPage");
  }

  configuration_page(){
    this.navCtrl.push("ConfigurationPage");
  }

  search(){
    if(this.buscarProducto){
      this.navCtrl.push("ProductSearchPage",{keyword:this.buscarProducto});
    }
  }

}
