import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-delete-account',
  templateUrl: 'delete-account.html',
})
export class DeleteAccountPage {

  userData:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public cancelacion: GeneralQueryProvider) {
    this.userData = this.navParams.get('datos');
  }

  confirmarEliminacion(){
    this.cancelacion.getUserId().then((id)=>{
      this.cancelacion.cancelarCuenta(id).subscribe(Resultado =>{
        this.navCtrl.setRoot('DeleteSuccessPage');
      }); 
    });
  }

}
