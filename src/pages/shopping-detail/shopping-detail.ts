import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { CallNumber } from '@ionic-native/call-number';
import { PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-shopping-detail',
  templateUrl: 'shopping-detail.html',
})
export class ShoppingDetailPage {

  public products:any;
  public fechaCreacion:any;
  public codigoCompra:any;
  public order:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public orders: GeneralQueryProvider,
    private callNumber: CallNumber,public popoverCtrl: PopoverController,public alertController: AlertController) {
    this.products = [];
    this.products = this.navParams.get('productos');
    this.order = this.navParams.get('orderDetail');
  }


  cancelarCompra(codigoCompra){
    this.orders.presentLoading();
    this.orders.cancelOrder(codigoCompra).subscribe(Resultado =>{
      
    });
    this.orders.dismissLoading();
    this.navCtrl.setRoot('OrderPage');
  }

  llamarRepartidor(telefonoRepartidor){
    this.callNumber.callNumber(telefonoRepartidor, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  enviarMensaje(telefonoRepartidor){
    this.navCtrl.push("SendMessagePage",{delivery_person_phone:telefonoRepartidor});
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("PopoverPage");
    popover.present({
      ev: myEvent
    });
  }

  async presentAlertConfirm(codigo) {
    const alert = await this.alertController.create({
      message: '<strong>¿Deseas cancelar la compra?</strong>',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'botonCancelar',
          handler: (blah) => {
          }
        }, {
          text: 'SI',
          cssClass: 'botonAceptar',
          handler: () => {
            this.cancelarCompra(codigo);
          }
        }
      ]
    });

    await alert.present();
  }


}
