import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductPageDetailPage } from './product-page-detail';

@NgModule({
  declarations: [
    ProductPageDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductPageDetailPage),
  ],
})
export class ProductPageDetailPageModule {}
