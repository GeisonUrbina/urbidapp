import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-product-page-detail',
  templateUrl: 'product-page-detail.html',
  providers:[GeneralQueryProvider]
})
export class ProductPageDetailPage {

   public producto:any;
   public cantidad:any;
   public isFavorite:any;
   
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public provider: GeneralQueryProvider) {
    this.producto = this.navParams.get('product');
    this.provider.getUserId().then((id)=>{
      this.provider.isProductFavorite(id,this.producto.product_code).then(val => {
        this.isFavorite = val;
      });
    });
  }

 agregar_producto_cesta(codigo_producto){
  this.provider.getUserId().then((id) => {
    this.provider.isProductInCart(id,codigo_producto).then(val => {
      if(!val){
  
        this.provider.getUserId().then((id) => {
          this.provider.saveProductsInsideCart(id,codigo_producto,1).then((val) => {
            this.provider.presentToast('Producto agregado al carrito');
          }).catch(err => {
            this.provider.presentToast('Producto no se ha agregado al carrito');
          });
        });
      }else{
        this.provider.presentToast('Producto ya ha sido agregado al carrito');
      }
    }).catch(err => {
      this.provider.presentToast('Producto no se ha podido agregar al carrito');
    });
  });
 }

 agregar_producto_favorito(codigo_producto){
  this.provider.getUserId().then((id)=>{
    this.provider.isProductFavorite(id,codigo_producto).then(val => {
      if(!val){
        this.provider.getUserId().then((id) => {
          this.provider.saveProductInFavorite(id,codigo_producto,1).then(val => {
            this.isFavorite = true;
            this.provider.presentToast('Producto agregado a favoritos');
          }).catch(err => {
            this.isFavorite = false;
            this.provider.presentToast('Producto no se ha agregado a favoritos');
          });
        });
      }else{
        this.provider.presentToast('Producto ya ha sido agregado a favoritos');
      }
    }).catch(err => {
      this.provider.presentToast('Producto ya ha sido agregado a favoritos');
    });

  });
 }


 eliminar_producto_favorito(codigo_producto){
  this.provider.getUserId().then((id) => {
    this.provider.deleteProductInFavorite(id,codigo_producto).then(()=>{
      this.isFavorite = false;
     }).catch(() => {
       alert("error eliminar");
     });
  });
}

}
