import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import { AlertController } from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  Marker
} from '@ionic-native/google-maps';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-ubication',
  templateUrl: 'ubication.html',
})
export class UbicationPage {

  latitud:any;
  longitud:any;
  isMapLoaded:boolean;
  map: GoogleMap;
  montoPagoIngresado:any;
  costoEnvio:any;
  formaPago:any;
  constructor(private platform:Platform,private alertCtrl: AlertController,public navCtrl: NavController, 
    public navParams: NavParams, private geo:Geolocation,public shopping:GeneralQueryProvider,private storage:Storage) {
    this.montoPagoIngresado = this.navParams.get('montoPagoIngresado');
    this.costoEnvio = this.navParams.get('costoEnvio');
    this.formaPago = this.navParams.get('formaPago');
    this.isMapLoaded=false;
    this.platform.ready().then(() =>{
      let options = {
        timeout:10000,
        enableHighAccuracy:true
      };
      this.geo.getCurrentPosition().then((pos) =>{
        this.latitud = pos.coords.latitude;
        this.longitud = pos.coords.longitude;
        this.loadMap();
        this.isMapLoaded=true;
      }).catch(err => this.presentAlert(err.message));

    });
  }

  presentAlert(err) {
    let alert = this.alertCtrl.create({
      title: 'Low battery',
      subTitle: this.latitud + 'prueba' + err,
      buttons: ['Dismiss']
    });
    alert.present();
  }
  
  

  ionViewDidLoad() {
   
    
  }

  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: this.latitud,
           lng: this.longitud
         },
         zoom: 18,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);

    let marker: Marker = this.map.addMarkerSync({
      title: 'Su lugar de entrega',
      icon: '#00838f',
      animation: 'DROP',
      draggable:true,
      position: {
        lat: this.latitud,
        lng: this.longitud
      }
    });

    marker.on(GoogleMapsEvent.MARKER_DRAG_END).subscribe(()=>{
      let markerlatlong = marker.getPosition();
      this.latitud = markerlatlong.lat;
      this.longitud = markerlatlong.lng;
    });
  }

  realizarCompra(){
    this.storage.get("cart").then(carrito => {
      this.shopping.getUserId().then((id) => {
        this.shopping.procesarCompra(this.latitud,this.longitud,this.montoPagoIngresado,carrito,this.costoEnvio,this.formaPago,id).subscribe(response =>  {
          this.navCtrl.setRoot("ShoppingSuccessPage");
        });
      });
    });
  }

}
