import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  public notificaciones:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
    this.notificaciones = [];
  }

  ionViewDidLoad() {

  }

  mostrarContenido(){
    let notificacionModal = this.modalCtrl.create('NotificationDetailPage');
    notificacionModal.present(); 
  }

  consultarNotificaciones(){

  }
  
}
