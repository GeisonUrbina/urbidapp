import { Component } from '@angular/core';
import { IonicPage,NavController } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[GeneralQueryProvider]
})
export class HomePage {

  phone: AbstractControl;
  loginForm: FormGroup;
  mensajeError: String;

  //Facebook
  userData:any ={};
  isUserLoggedIn:any = false;
  constructor(public navCtrl: NavController,public userSignup: GeneralQueryProvider,
    private formBuilder: FormBuilder,private fb: Facebook,private storage: Storage) {
    this.mensajeError = "";
    this.loginForm = this.formBuilder.group({
            phone: ['', [Validators.required,Validators.maxLength(8),Validators.minLength(8)]]
        });
  }

  signin_request(value: any): void { 
        if(this.loginForm.valid) {
          this.userData.client_phone = value.phone;
          this.verificarExistencia("T");
        }else{
          this.mensajeError = "El número celular está vacío o es inválido";
        }
  }



  signup_page(){
  	this.navCtrl.push("SignupPage");
  }

  iniciarSesionFacebook(){
    this.fb.login(['public_profile', 'user_friends', 'email'])
    .then((res: FacebookLoginResponse) =>{
      if(res.status === 'connected'){
        this.fb.api('me?fields=id,last_name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
          this.userData.email=profile['email'];
          this.userData.first_name = profile['first_name'];
          this.userData.last_name= profile['last_name'];
          this.userData.id=profile['id'];
          this.verificarExistencia("F");
        });
      }else{
      }
    })
    .catch(e => this.navCtrl.push("PhoneVerificationPage"));
  }

  verificarExistencia(tipoLogin){
    this.userSignup.presentLoading();
    switch(tipoLogin){
      case "F":
        this.userSignup.estaRegistrado(tipoLogin,this.userData).subscribe(Resultado =>{
          this.userSignup.dismissLoading();
          if(Resultado["status_response"] == "NO_CREADO"){
            this.navCtrl.push("SignupPage",{userData:this.userData,createType:tipoLogin});
          }else{
            let usuario = Resultado['user'];
            this.userSignup.obtenerToken(usuario.client_phone).subscribe(Telefono => {
              this.userData.smsToken = Telefono["token_sms"];
              this.userData.phone = usuario.client_phone;
              this.userData.name = usuario.client_name;
              this.userData.id = usuario.id;
              this.userData.action="LOGIN";
              this.navCtrl.push("PhoneVerificationPage",{userData:this.userData});
            });
          }
        });
        break;
      case "T":
        this.userSignup.estaRegistrado(tipoLogin,this.userData).subscribe(Resultado =>{
          this.userSignup.dismissLoading();
          if(Resultado["status_response"] == "NO_CREADO"){
            this.navCtrl.push("SignupPage",{userData:this.userData,createType:tipoLogin});
          }else{
            let usuario = Resultado['user'];
            this.userSignup.obtenerToken(usuario.client_phone).subscribe(Telefono => {
                this.userData.smsToken = Telefono["token_sms"];
                this.userData.phone = usuario.client_phone;
                this.userData.name = usuario.client_name;
                this.userData.id = usuario.id;
                this.userData.action="LOGIN";
                this.navCtrl.push("PhoneVerificationPage",{userData:this.userData});
            });
          }
        });
      break;
    }
  }
  
  cerrarSessionFacebook(){
    this.fb.logout().then(Res => this.isUserLoggedIn=false).catch(e=>console.log(e));
  }
 

}
