import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';


@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
  providers:[GeneralQueryProvider]
})
export class CategoryPage {
  
  public categorias:any;
  public mensaje:any;
  
  @ViewChild(Slides) slides: Slides;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public categories: GeneralQueryProvider) {
    this.categorias = [];
  }

  ionViewDidLoad() {
    this.categories.presentLoading();
     this.categories.getCategories().subscribe(Cates => {
      Cates.map(Cate => {
        this.categorias.push(Cate);
      });
     });
     this.categories.dismissLoading();
  }
  goToSlide() {
    this.slides.slideTo(2, 500);
  }

  subcategory_page(category_code){
    this.navCtrl.push("SubcategoryPage",{category_code:category_code});
    //this.navCtrl.push("UbicationPage");
  }

  
 
}
