import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  public codigo_producto:string;
  public cantidad:string;
  public products:any;
  public noProducts:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private storage:Storage,
    public viewCtrl: ViewController,public provider: GeneralQueryProvider) {
    this.products = [];
    this.noProducts=false;
    this.consultarProductosCesta();
  }

  delete_product_cart(codigo_producto){
    this.provider.getUserId().then((id) => {
      this.provider.deleteProductInsideCart(id,codigo_producto);
    for(let i=0;i<this.products.length;i++){
      if(this.products[i].product_code == codigo_producto){
          this.products.splice(i,1);
          break;
        }
      }
      if(this.products.length == 0){
        this.noProducts=true;
      }
    });
  }

  consultarProductosCesta(){
    this.provider.presentLoading();
    let productosGuardados ="";
    let arreglo = [];
    this.storage.get("cart").then(val => {
      if(val !== null){
        arreglo = JSON.parse(val);
        productosGuardados = JSON.stringify(arreglo);

        this.provider.getUserId().then((id)=>{
          this.provider.getCart(id,productosGuardados).subscribe(Productos => {
            Productos.map(Product => {
              this.products.push(Product);
            });
            if(Productos && Productos.length > 0){
              this.noProducts = false;
            }else{
              this.noProducts = true;
            }
          });
        });
      }
    });
    this.provider.dismissLoading();
  }
  generate_receipt(){
    this.navCtrl.push("ReceiptPage");
  }
  aumentarCantidadProducto(codigo_producto){
    this.provider.getUserId().then((id) => {
      for(let i=0;i< this.products.length;i++){
        if(this.products[i].product_code == codigo_producto){
          this.products[i].product_quantity +=1;
          this.provider.modifyProductCartQuantity(id,codigo_producto,this.products[i].product_quantity);
        }
      }
    });
  }
  disminuirCantidadProducto(codigo_producto){
    this.provider.getUserId().then((id) => {
      for(let i=0;i< this.products.length;i++){
        if(this.products[i].product_code == codigo_producto){
          if(this.products[i].product_quantity >1){
            this.products[i].product_quantity -=1;
            this.provider.modifyProductCartQuantity(id,codigo_producto,this.products[i].product_quantity);
            break;
          }
        }
      }

    });
  }
}
