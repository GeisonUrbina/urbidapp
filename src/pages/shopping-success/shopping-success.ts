import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-shopping-success',
  templateUrl: 'shopping-success.html',
})
export class ShoppingSuccessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShoppingSuccessPage');
  }

  verPedidosRealizados(){
    this.navCtrl.setRoot("OrderPage");
  }

}
