import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoppingSuccessPage } from './shopping-success';

@NgModule({
  declarations: [
    ShoppingSuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoppingSuccessPage),
  ],
})
export class ShoppingSuccessPageModule {}
