import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
})
export class FavoritePage {

  public products:any;
  public noProducts:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage:Storage,
    public provider: GeneralQueryProvider) {
    this.products = [];
    this.noProducts = false;
    this.consultarProductosFavorito();
  }


  agregar_producto_cesta(codigo_producto){
    this.provider.getUserId().then((id) => {
      this.provider.isProductInCart(id,codigo_producto).then(val => {
        if(!val){
          this.provider.getUserId().then((id) => {
            this.provider.saveProductsInsideCart(id,codigo_producto,1).then((val) => {
              this.provider.presentToast('Producto agregado al carrito');
            }).catch(err => {
              this.provider.presentToast('Producto no se ha agregado al carrito');
            });
          });
        }else{
          this.provider.presentToast('Producto ya ha sido agregado al carrito');
        }
      }).catch(err => {
        this.provider.presentToast('Producto no se ha podido agregar al carrito');
      });
    });
   }

  eliminar_producto_favorito(codigo_producto){
    this.provider.getUserId().then((id) => {
      this.provider.deleteProductInFavorite(id,codigo_producto);
      for(let i=0;i<this.products.length;i++){
        if(this.products[i].product_code == codigo_producto){
          this.products.splice(i,1);
          break;
        }
      }
      if(this.products.length == 0){
        this.noProducts=true;
      }
    });
  }

  consultarProductosFavorito(){
    this.provider.presentLoading();
    let productosGuardados ="";
    let arreglo = [];
    this.storage.get("favorite").then(val => {
      if(val !== null){
        arreglo = JSON.parse(val);
        productosGuardados = JSON.stringify(arreglo);
        if(productosGuardados === ""){
          this.products = [];
        }
        this.provider.getUserId().then((id)=>{
          this.provider.getCart(id,productosGuardados).subscribe(Productos => {
            Productos.map(Product => {
              this.products.push(Product);
            });
            if(Productos && Productos.length > 0){
              this.noProducts = false;
            }else{
              this.noProducts = true;
            }
          });
        });
      }
    });
    this.provider.dismissLoading();
    
  }

}
