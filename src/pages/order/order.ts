import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { EnumProvider } from '../../providers/enum/enum';
import {Storage} from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

  public myOrders:any;
  public mensaje:any;
  public productos:any;
  public urlIP:String;
  public noOrders:any;
  constructor(public http: Http,public navCtrl: NavController, public navParams: NavParams,
    public orders: GeneralQueryProvider,public enumerados:EnumProvider,private storage:Storage) {
    this.myOrders = [];
    this.productos = [];
    this.urlIP = 'www.urbidcr.com';
    this.noOrders = false;
  }

  ionViewDidLoad() {
    this.orders.presentLoading();

    this.storage.get("user_id").then((val)=>{
      let x = new FormData();
      x.append('user_id',val);
      this.http.post("http://"+this.urlIP+"/administration/order/orderList",x).map(res=>res.json()).subscribe(Pedidos => {
        Pedidos.map(Pedido =>{
          Pedido.status = this.enumerados.enumStatus(Pedido.status);
          Pedido.forma_pago = this.enumerados.enumPaymentType(Pedido.forma_pago);
          this.myOrders.push(Pedido);
        });
        if(Pedidos && Pedidos.length > 0){
          this.noOrders = false;
        }else{
          this.noOrders = true;
        }
      });
    });
    this.orders.dismissLoading();
  }

  verProductosCompra(order){
    this.productos = [];
    this.orders.getUserId().then((id)=>{
      this.orders.getCart(id,order.carrito).subscribe(Prots => {
        Prots.map(p => {
          this.productos.push(p);
        });
      });
    });
    
    this.navCtrl.push("ShoppingDetailPage",{productos:this.productos,orderDetail:order});
  }
}
