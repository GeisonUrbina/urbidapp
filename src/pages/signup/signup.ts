import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

	  user_name: AbstractControl;
    user_surname: AbstractControl;
    user_email: AbstractControl;
    user_phone: AbstractControl;
    user_password: AbstractControl;
    signupForm: FormGroup;
    userData:any ={};
    createType:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public userSignup: GeneralQueryProvider,private formBuilder: FormBuilder) {
    this.userData = this.navParams.get('userData');
    this.createType = this.navParams.get('createType');
    
    if(this.userData != null && this.createType == 'F'){
      this.user_name = this.userData.first_name;
      this.user_surname = this.userData.last_name;
      this.user_email = this.userData.email;
      this.signupForm = this.formBuilder.group({
        user_name: ['', Validators.required],
        user_surname: ['', Validators.required],
        user_email: ['', Validators.required],
        user_phone: ['', [Validators.required,Validators.minLength(8),Validators.maxLength(8),Validators.pattern('[0-9]+')]],
    });
    }else{
      this.user_phone = this.userData!=null ? this.userData.client_phone:" ";
      this.signupForm = this.formBuilder.group({
        user_name: ['', Validators.required],
        user_surname: ['', Validators.required],
        user_email: ['', [Validators.required,Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]],
        user_phone: ['', [Validators.required,Validators.minLength(8),Validators.maxLength(8),Validators.pattern('[0-9]+')]],
        user_password: ['',[Validators.required,Validators.minLength(8)]]
      });
      if(this.userData==null){
        this.navCtrl.setRoot("HomePage");
      }
    }
  }

  

  signup_request(value: any): void { 
      
        if(this.signupForm.valid) {
             this.userSignup.presentLoading();
             this.userSignup.obtenerToken(value.user_phone).subscribe(Telefono => {
             this.userSignup.dismissLoading();
             this.userData.smsToken = Telefono["token_sms"];
             this.userData.createType = this.createType;
             this.userData.first_name = value.user_name;
             this.userData.last_name = value.user_surname;
             this.userData.email = value.user_email
             this.userData.phone = value.user_phone;
             if(this.createType == 'T'){
              this.userData.password = value.user_password;
              this.userData.id = "";
             }else{
              this.userData.password = "";
             }
             this.userData.password = value.user_password;
             this.userData.action="SIGNUP";
             this.navCtrl.push("PhoneVerificationPage",{userData:this.userData});
           });
        }
  }


}
