import { Component } from '@angular/core';
import { IonicPage,ModalController, NavController, NavParams,ViewController } from 'ionic-angular';
import {ProductPageDetailPage} from '../product-page-detail/product-page-detail';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
  providers:[GeneralQueryProvider]
})
export class ProductsPage {

  public productos:any;
  public subcategory_code:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public viewCtrl: ViewController,public provider: GeneralQueryProvider) {
        this.productos = [];
        this.subcategory_code = this.navParams.get('subcategoria_code');
  }

  ionViewDidLoad() {
    this.provider.presentLoading();
    this.provider.getProducts(this.subcategory_code).subscribe(Products => {
      Products.map(Product => {
        this.productos.push(Product);
      });
     });
     this.provider.dismissLoading();
  }

  presentProfileModal(product) {
   this.navCtrl.push("ProductPageDetailPage",{product:product});
 }

 agregar_producto_cesta(codigo_producto,cantidad){  
   this.provider.getUserId().then((id) => {
    this.provider.isProductInCart(id,codigo_producto).then(val => {
      if(!val){
        this.provider.getUserId().then((id) =>{
          this.provider.saveProductsInsideCart(id,codigo_producto,1).then((val) => {
            this.provider.presentToast('Producto agregado al carrito');
          }).catch(err => {
            this.provider.presentToast('Producto no se ha agregado al carrito');
          });
        });
      }else{
        this.provider.presentToast('Producto ya ha sido agregado al carrito');
      }
    }).catch(err => {
      this.provider.presentToast('Producto no se ha podido agregar al carrito');
    });
   });
 }

}
