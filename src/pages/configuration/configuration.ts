import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController  } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-configuration',
  templateUrl: 'configuration.html',
})
export class ConfigurationPage {

  userData:any;
  user_name: string;
  user_surname: string;
  user_email: string;
  user_phone: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController,
    public user: GeneralQueryProvider) {
      
      this.user.getUserId().then((id)=>{
        this.user.datosCliente(id).subscribe(Data => {
          this.userData = Data['data'];
          this.user_name = this.userData.client_name;
          this.user_surname = this.userData.client_lastName;
          this.user_email = this.userData.email;
          this.user_phone = this.userData.client_phone;
        });
      });
  }

  modificar(tipo){
    switch(tipo){
      case 'N':
        this.navCtrl.push('GenericConfigurationPage',{tipo:tipo,valor:this.user_name,titulo:"Nombre"});
        break;
      case 'A':
        this.navCtrl.push('GenericConfigurationPage',{tipo:tipo,valor:this.user_surname,titulo:"Apellido"});
        break;
      case 'T':
        this.navCtrl.push('GenericConfigurationPage',{tipo:tipo,valor:this.user_phone,titulo:"Teléfono"});
        break;
      case 'C':
        this.navCtrl.push('GenericConfigurationPage',{tipo:tipo,valor:this.user_email,titulo:"Correo electrónico"});
        break;
    }
  }

  eliminarCuenta(){
    this.navCtrl.push('DeleteAccountPage',{datos:this.userData});
  }

}
