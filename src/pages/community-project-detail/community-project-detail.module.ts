import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommunityProjectDetailPage } from './community-project-detail';

@NgModule({
  declarations: [
    CommunityProjectDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CommunityProjectDetailPage),
  ],
})
export class CommunityProjectDetailPageModule {}
