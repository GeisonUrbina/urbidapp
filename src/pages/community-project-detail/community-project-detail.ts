import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-community-project-detail',
  templateUrl: 'community-project-detail.html',
})
export class CommunityProjectDetailPage {

  public proyecto:any;
  public mensaje:any;
  public isAffiliated:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public afiliacion:GeneralQueryProvider) {
    this.proyecto = this.navParams.get('proyecto');
    this.isAffiliated=false;
    this.esAfiliado(this.proyecto.project_code);
  }

  ionViewDidLoad() {
    
  }

  afiliarse(codigoProyecto){

    this.afiliacion.modificarAfiliacion(codigoProyecto).subscribe(Resultado =>{
      this.isAffiliated=true;
    });
  }

  desafiliarse(){
    this.afiliacion.desafiliarProyecto().subscribe(Resultado => {
      this.isAffiliated=false;
    });
  }

  esAfiliado(codigoProyecto){
    this.afiliacion.estaAfiliado(codigoProyecto).subscribe(Resultado => {
      if(Resultado["status_response"] == "AFILIADO"){
        this.isAffiliated=true;
      }
    });
  }

}
