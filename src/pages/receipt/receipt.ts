import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-receipt',
  templateUrl: 'receipt.html',
})
export class ReceiptPage {
  private recibo:any;
  public montoTotal:number;
  public costoEnvio:number;
  public montoAporteTotal:number;
  public montoPago:any;
  public OMI:any;
  public errorMonto:any;
  public montoPagoIngresado:number;
  public formaPago:any;
  public puntosCliente:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public provider:GeneralQueryProvider,private storage:Storage) {
    this.recibo =[];
    this.montoTotal=0;
    this.costoEnvio=0;
    this.puntosCliente=0;
    this.montoAporteTotal=0;
    this.OMI = false;
    this.errorMonto = false;
    this.formaPago="EF";
  }

  ionViewDidLoad() {
    let productosGuardados ="";
    let arreglo = [];
    this.storage.get("cart").then(val => {
      if(val !== null){
        arreglo = JSON.parse(val);
        productosGuardados = JSON.stringify(arreglo);

        this.provider.getUserId().then((id) => {
          this.provider.getReceipt(productosGuardados,id).subscribe(Productos => {
            Productos["productos"].map(Product => {
              this.recibo.push(Product);
            });
            this.montoTotal = Productos["montoTotal"];
            this.costoEnvio = Productos["costoEnvio"];
            this.montoAporteTotal = Productos["montoAporteTotal"];
            this.puntosCliente = Productos["puntos"];
          });
        });
      }
    });
  }

ocultarInput(){
  this.montoPagoIngresado = this.montoTotal;
  this.OMI=false;
  this.errorMonto = false;
}

mostrarInput(){
  this.montoPagoIngresado = 0;
  this.OMI=true;
  this.errorMonto = false;
  
}

  geolocation_map(){
    if(this.OMI){
      if(this.montoPagoIngresado < this.montoTotal){
        this.errorMonto = true;
      }else{
        this.navCtrl.push("UbicationPage",{montoPagoIngresado:this.montoPagoIngresado,costoEnvio:this.costoEnvio,formaPago:"E"});
        this.errorMonto = false;
      }
    }else if(this.formaPago == "PU"){
      if(this.montoTotal <= this.puntosCliente){
        this.navCtrl.push("UbicationPage",{montoPagoIngresado:this.puntosCliente,costoEnvio:this.costoEnvio,formaPago:"P"});
      }
    }else if(this.formaPago == "SM"){
      this.navCtrl.push("UbicationPage",{montoPagoIngresado:this.montoTotal,costoEnvio:this.costoEnvio,formaPago:"S"});
    }else{
      this.navCtrl.push("UbicationPage",{montoPagoIngresado:this.montoTotal,costoEnvio:this.costoEnvio,formaPago:"E"});
    }
  }

}
