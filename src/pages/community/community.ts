import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-community',
  templateUrl: 'community.html',
})
export class CommunityPage {

  public proyectos:any;
  public noProjects:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public projects: GeneralQueryProvider) {
    this.proyectos = [];
    this.noProjects=false;
  }

  ionViewDidLoad() {
    this.projects.presentLoading();
    this.projects.getProjects().subscribe(Project => {
      Project.map(pro => {
        this.proyectos.push(pro);
      });
      if(Project && Project.length > 0){
        this.noProjects = false;
      }else{
        this.noProjects = true;
      }
     });
     this.projects.dismissLoading();
  }

  verDetalle(proyecto){
    this.navCtrl.push("CommunityProjectDetailPage",{proyecto:proyecto});
  }

}
