import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-delete-success',
  templateUrl: 'delete-success.html',
})
export class DeleteSuccessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage) {
    this.storage.remove("user_id");
    this.storage.remove("user_name");
    
  }

  aceptar(){
    this.navCtrl.setRoot("HomePage");
  }

}
