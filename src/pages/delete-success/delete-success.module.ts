import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeleteSuccessPage } from './delete-success';

@NgModule({
  declarations: [
    DeleteSuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(DeleteSuccessPage),
  ],
})
export class DeleteSuccessPageModule {}
