import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { MenuController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-subcategory',
  templateUrl: 'subcategory.html',
  providers:[GeneralQueryProvider]
})
export class SubcategoryPage {

  public subcategorias:any;
  public category_code:string;

  constructor(public navCtrl: NavController, public menu: MenuController,public navParams: NavParams,public subcategories: GeneralQueryProvider) {
  this.subcategorias = [];
  this.category_code = this.navParams.get('category_code');
  this.menu.enable(true);
  }

  ionViewDidLoad() {
    this.subcategories.presentLoading();
    this.subcategories.getSubCategories(this.category_code).subscribe(Subcates => {
      Subcates.map(Subcate => {
        this.subcategorias.push(Subcate);
      });
     });
     this.subcategories.dismissLoading();
  }

  product_page(subcategoria_code){
  
    this.navCtrl.push("ProductsPage",{subcategoria_code:subcategoria_code});
  }

}
