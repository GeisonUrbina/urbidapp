import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-phone-verification',
  templateUrl: 'phone-verification.html',
})
export class PhoneVerificationPage {

  phone_code:AbstractControl;
  signupForm: FormGroup;
  userData: any = {};
  cantidadIntentos:number = 3;
  mensajeIntentos:string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,public provider: GeneralQueryProvider,
    private formBuilder: FormBuilder,private storage:Storage) {
    this.userData = this.navParams.get('userData');
    this.signupForm = this.formBuilder.group({
      phone_code: ['', [Validators.required,Validators.minLength(6),Validators.maxLength(6)]]
  });
  }

  combrobarToken(value: any): void{
    if(this.signupForm.valid) {
      if(value.phone_code == this.userData.smsToken){
        if(this.userData.action === "SIGNUP"){
          this.registrarse();
        }else if(this.userData.action === "LOGIN"){
          this.ingresarPlataforma();
        }else{
          this.modificarTelefono();
        }
      }else{
        this.cantidadIntentos -= 1;
        this.mostrarMensajeIntentos();
      }
    }
  }

  registrarse(){
    this.storage.get("user_device").then((val)=>{
      this.provider.postSignup(this.userData.first_name,this.userData.last_name,this.userData.email,this.userData.phone,this.userData.password,this.userData.createType,this.userData.id,val).subscribe(User => {
        if(User.clientAutenticado === "FAILURE"){
              alert("Se ha presentado un error");
            }else{
              this.storage.set("user_id",User.cliente.id);
              this.storage.set("user_name",User.cliente.client_name);
              this.navCtrl.setRoot("SideMenuPage");
        }
      });
    });
  }

  modificarTelefono(){
    this.provider.actualizarTelefono(this.userData.phone,this.userData.id).subscribe(Resultado => {
      this.provider.presentToast('Teléfono actualizado');
      this.navCtrl.setRoot("ConfigurationPage");
    });
  }

  mostrarMensajeIntentos(){
    if(this.cantidadIntentos === 0){
      this.navCtrl.pop();
    }else{
      this.cantidadIntentos === 1 ? this.mensajeIntentos = "Usted tiene " + this.cantidadIntentos + " intento restante.": this.mensajeIntentos = "Usted tiene " + this.cantidadIntentos + " intentos restantes.";
    }
  }

  ingresarPlataforma(){
    this.storage.get("user_device").then((val)=>{
      this.provider.actualizarDevice(this.userData.id,val).subscribe(Resultado => {
        this.storage.set("user_id",this.userData.id);
        this.storage.set("user_name",this.userData.name);
        this.navCtrl.setRoot("SideMenuPage");

      });
    });
  }

}
