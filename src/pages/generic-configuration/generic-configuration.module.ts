import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GenericConfigurationPage } from './generic-configuration';

@NgModule({
  declarations: [
    GenericConfigurationPage,
  ],
  imports: [
    IonicPageModule.forChild(GenericConfigurationPage),
  ],
})
export class GenericConfigurationPageModule {}
