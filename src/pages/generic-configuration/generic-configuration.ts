import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-generic-configuration',
  templateUrl: 'generic-configuration.html',
})
export class GenericConfigurationPage {

  public tituloLabel:any;
  public valor:any;
  public tipo:any;
  mensajeError: String;
  userData:any ={};

  //campos
  nombre:AbstractControl;
  apellido:AbstractControl;
  telefono:AbstractControl;
  correo:AbstractControl;
  configurationForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder,
    private provider: GeneralQueryProvider) {
    this.tipo = this.navParams.get('tipo');
    this.valor = this.navParams.get('valor');
    this.tituloLabel = this.navParams.get('titulo');
    this.mensajeError = "El campo está vacío o es inválido";
    switch(this.tipo){
      case 'N':
          this.configurationForm = this.formBuilder.group({
            nombre: ['', [Validators.required]]
          });
        break;
      case 'A':
          this.configurationForm = this.formBuilder.group({
            apellido: ['', [Validators.required]]
          });
        break;
      case 'T':
          this.configurationForm = this.formBuilder.group({
            telefono: ['', [Validators.required,Validators.minLength(8),Validators.maxLength(8),Validators.pattern('[0-9]+')]]
          });
        break;
      case 'C':
          this.configurationForm = this.formBuilder.group({
            correo: ['', [Validators.required,Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]]
          });
        break;
    }

  }

  modificar(value: any): void{
    this.provider.getUserId().then((id)=>{
      if(this.configurationForm.valid) {
        switch(this.tipo){
          case 'N':
            this.provider.actualizarNombre(value.nombre,id).subscribe(Resultado => {
              this.provider.presentToast('Nombre actualizado');
              this.navCtrl.setRoot("ConfigurationPage");
            });
            break;
          case 'A':
            this.provider.actualizarApellido(value.apellido,id).subscribe(Resultado => {
              this.provider.presentToast('Apellido actualizado');
              this.navCtrl.setRoot("ConfigurationPage");
            });
            break;
          case 'T':
              this.provider.obtenerToken(value.telefono).subscribe(Telefono => {
                this.userData.smsToken = Telefono["token_sms"];
                this.userData.phone = value.telefono;
                this.userData.id = id;
                this.userData.action="PHONE_MODIFY";
                this.navCtrl.push("PhoneVerificationPage",{userData:this.userData});
              });
            break;
          case 'C':
            this.provider.actualizarCorreo(value.correo,id).subscribe(Resultado => {
              this.provider.presentToast('Correo electrónico actualizado');
              this.navCtrl.setRoot("ConfigurationPage");
            });
            break;
        }

      }else{
        this.mensajeError = "El campo está vacío o es inválido";
      }
      
    });
  }

}
