import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralQueryProvider } from '../../providers/general-query/general-query';

@IonicPage()
@Component({
  selector: 'page-product-search',
  templateUrl: 'product-search.html',
})
export class ProductSearchPage {

  keyword:any;
  public productos:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public provider: GeneralQueryProvider) {
    this.productos = [];
    this.keyword = this.navParams.get('keyword');
  }

  ionViewDidLoad() {
    this.provider.presentLoading();
    this.provider.searchProducts(this.keyword).subscribe(Products => {
      Products.map(Product => {
        this.productos.push(Product);
      });
     });
     this.provider.dismissLoading();
  }

  presentProfileModal(product) {
    this.navCtrl.push("ProductPageDetailPage",{product:product});
  }

  agregar_producto_cesta(codigo_producto,cantidad){
    this.provider.getUserId().then((id) => {
      this.provider.isProductInCart(id,codigo_producto).then(val => {
        if(!val){
          this.provider.getUserId().then((id) => {
            this.provider.saveProductsInsideCart(id,codigo_producto,1).then((val) => {
              this.provider.presentToast('Producto agregado al carrito');
            }).catch(err => {
              this.provider.presentToast('Producto no se ha agregado al carrito');
            });
          });
        }else{
          this.provider.presentToast('Producto ya ha sido agregado al carrito');
        }
      }).catch(err => {
        this.provider.presentToast('Producto no se ha podido agregar al carrito');
      });
    });
   }

}
