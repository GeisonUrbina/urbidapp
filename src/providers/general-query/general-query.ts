import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import {Storage} from '@ionic/storage';


@Injectable()
export class GeneralQueryProvider {

	public urlCategorias:string;
  public urlSubcategorias:string;
  public urlProductos:string;
  public urluserLoginAPI:string;
  public urluserSignupAPI:string;
  public urlcart:string;
  public urlreceipt:string;
  public urlGenerateShopping:string;
  public urlProjects:string
  public urlIP:string;
  public loader:any;
  public createAPI:string;
  public facebookAPI:string;
  public token:string;
  private id:string;

  constructor(public http: Http,public loadingCtrl: LoadingController,private toastCtrl: ToastController,
    private storage:Storage) {

    this.urlCategorias='/categoriasAPI';
    this.urlSubcategorias='/subcategoriasAPI';
    this.urlProductos='/productosAPI';
    this.urluserLoginAPI='/userLoginAPI';
    this.urluserSignupAPI='/userSignupAPI';
    this.urlcart='/cartAPI';
    this.urlreceipt='/receiptAPI';
    this.urlGenerateShopping='/generateShoppingAPI';
    this.urlProjects='/projectsAPI';
    this.createAPI="/createAPI";
    this.facebookAPI="/facebookAPI";
    this.token="/tokenAPI";
    this.urlIP = 'www.urbidcr.com';
  }

  getProjects(){return this.http.get("http://"+this.urlIP+"/app/project/projects").map(res => res.json());}


  getCategories(){return this.http.get("http://"+this.urlIP+"/app/category/categories").map(res => res.json());}

  getSubCategories(category_code){return this.http.get("http://"+this.urlIP+"/app/subcategory/subcategories/"+category_code).map(res => res.json());}

  getProducts(subcategory_code){
    return this.http.get("http://"+this.urlIP+"/app/product/products/" + subcategory_code).map(res => res.json());
  }

  getLogin(user,pass){
    let loginForm = new FormData();
    loginForm.append('client_email',user);
    loginForm.append('client_password',pass);
    console.log(loginForm);
    return this.http.post(this.urluserLoginAPI,loginForm).map(res => res.json());
  }

  postSignup(user_name,user_surname,user_email,user_phone,user_password,create_type,user_id,user_device){
    let signupForm = new FormData();
    signupForm.append('user_name',user_name);
    signupForm.append('user_surname',user_surname);
    signupForm.append('user_email',user_email);
    signupForm.append('user_phone',user_phone);
    signupForm.append('user_password',user_password);
    signupForm.append('create_type',create_type);
    signupForm.append('user_id',user_id);
    signupForm.append('user_device',user_device);
    return this.http.post("http://"+this.urlIP+"/app/client/new-client-app",signupForm).map(res => res.json());
  }

  getCart(id_cliente,cart_products){
    let cartForm = new FormData();
    cartForm.append('cart_products',cart_products);
    cartForm.append('cliente',id_cliente);
    return this.http.post("http://"+this.urlIP+"/app/cart/cart-products",cartForm).map(res => res.json());
  }
  
  getReceipt(cart_products,idCliente){
    let cartForm = new FormData();
    cartForm.append('cart_products',cart_products);
    cartForm.append('id_cliente',idCliente);
    return this.http.post("http://"+this.urlIP+"/app/receipt/receipt-list",cartForm).map(res => res.json());
  }

  saveProductsInsideCart(id_cliente,codigo_producto, cantidad){
    let productosGuardados = [];
    return new Promise((resolve) =>{
      this.storage.get("cart").then(val => {
        if(val === null) productosGuardados = JSON.parse("[]");
        else productosGuardados = JSON.parse(val);
        productosGuardados.push({"cliente":id_cliente,"codigo":codigo_producto,"cantidad":cantidad});
        this.storage.set("cart",JSON.stringify(productosGuardados)).then(val => {
          resolve();
        });
      });
    });
  }

  deleteProductInsideCart(idCliente,codigo_producto){
    let productosGuardados = [];
    return new Promise((resolve,reject) => {
      this.storage.get("cart").then(val => {
        productosGuardados = JSON.parse(val);
        for(let i=0;i<productosGuardados.length;i++){
          if(productosGuardados[i].cliente === idCliente && productosGuardados[i].codigo === codigo_producto){
              productosGuardados.splice(i,1);
              break;
          }
        }
        this.storage.set("cart",JSON.stringify(productosGuardados)).then(val => {
          resolve();
        }).catch(err => {
          reject();
        });
      }).catch(err => {
        reject();
      });
    });
  }

  saveProductInFavorite(id_cliente,codigo_producto,cantidad){
    let productosGuardados = [];
    return new Promise((resolve, reject) => {
      this.storage.get("favorite").then(val => {
        if(val === null){
          productosGuardados = JSON.parse("[]");
        }else{
          productosGuardados = JSON.parse(val);
        }
        
        productosGuardados.push({"cliente":id_cliente,"codigo":codigo_producto,"cantidad":cantidad});
        this.storage.set("favorite",JSON.stringify(productosGuardados)).then(val => {
          resolve();
        });
      }).catch(err => {
        productosGuardados.push({"codigo":codigo_producto,"cantidad":1});
        this.storage.set("favorite",JSON.stringify(productosGuardados)).then(val => {
          resolve();
        });
        
      });
    });
  }


  deleteProductInFavorite(idCliente,codigo_producto){
    let productosGuardados = [];
    return new Promise((resolve,reject)  => {
      this.storage.get("favorite").then(val => {
        productosGuardados = JSON.parse(val);
        for(let i=0;i<productosGuardados.length;i++){
          if(productosGuardados[i].cliente === idCliente && productosGuardados[i].codigo === codigo_producto){
              productosGuardados.splice(i,1);
              break;
          }
        }
        this.storage.set("favorite",JSON.stringify(productosGuardados)).then(val => {
          resolve();
        }).catch(err => {
          reject();
        });
      }).catch(err => {
        reject();
      });
    });   
  }


  modifyProductCartQuantity(idCliente,codigo_producto,cantidad){
    let productosGuardados = [];
      this.storage.get("cart").then(val => {
        productosGuardados = JSON.parse(val);
        for(let i=0;i<productosGuardados.length;i++){
          if(productosGuardados[i].cliente === idCliente && productosGuardados[i].codigo === codigo_producto){
            productosGuardados[i].cantidad = cantidad;
              break;
          }
        }
        this.storage.set("cart",JSON.stringify(productosGuardados)).then(val => {
        });
      });
    
  }

  procesarCompra(latitud,longitud,montoPagoIngresado,carrito,costoEnvio,formaPago,cliente){
    let cartForm = new FormData();
    cartForm.append('cart_products',carrito);
    cartForm.append('position_latitud',latitud);
    cartForm.append('position_longitud',longitud);
    cartForm.append('monto_pago_ingresado',montoPagoIngresado);
    cartForm.append('monto_envio',costoEnvio);
    cartForm.append('forma_pago',formaPago);
    cartForm.append('cliente',cliente);
    return this.http.post("http://"+this.urlIP+"/administration/shopping/order/processing",cartForm).map(res => res.json());
  }

  obtenerProductosCarritosLocal(){
    let productosGuardados ="";
    let arreglo = [];
    if(window.localStorage.getItem("cart")){
      arreglo = JSON.parse(window.localStorage.getItem("cart"));
      productosGuardados = JSON.stringify(arreglo);
    } 

    return productosGuardados;
  }

  isProductFavorite(idCliente,codigo_producto){
    let productosGuardados = [];
    return new Promise((resolve, reject) => {
      this.storage.get("favorite").then(val => {
        productosGuardados = JSON.parse(val);
        for(let i=0;i<productosGuardados.length;i++){
          if(productosGuardados[i].cliente === idCliente && productosGuardados[i].codigo === codigo_producto){
              resolve(true);
          }
        }
        resolve(false);
      }).catch(err => {
        resolve(false);
      });
    });
  }

  isProductInCart(idCliente,codigo_producto){
    let productosGuardados = [];
    return new Promise((resolve,reject) => {
      this.storage.get("cart").then(val => {
        productosGuardados = JSON.parse(val);
        for(let i=0;i<productosGuardados.length;i++){
          if(productosGuardados[i].cliente === idCliente && productosGuardados[i].codigo === codigo_producto){
              resolve(true);
          }
        }
        resolve(false);
      }).catch(() => {resolve(false);});
    });
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismiss();
  }

  presentToast(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 1700,
      position: 'bottom',
      cssClass: 'urbidToast'
    });
    toast.present();
  }


  cancelOrder(codigoCompra){
    let orderForm = new FormData();
    orderForm.append('orderCode',codigoCompra);
    return this.http.post("http://"+this.urlIP+"/administration/order/cancel",orderForm).map(res => res.json());
  }

  modificarAfiliacion(codigoProyecto){
    let modificar = new FormData();
    this.storage.get("user_id").then((val)=>{this.id = val});
    modificar.append('id_usuario',this.id);
    modificar.append('project_code',codigoProyecto);
   
    return this.http.post("http://"+this.urlIP+"/app/projects/affiliations",modificar).map(res => res.json());
  }

  desafiliarProyecto(){
    let desafiliar = new FormData();
    this.storage.get("user_id").then((val)=>{this.id = val}) ;
    desafiliar.append('id_usuario',this.id);

    return this.http.post("http://"+this.urlIP+"/app/projects/desaffiliation",desafiliar).map(res => res.json());
  }

  estaAfiliado(codigoProyecto){
    let esAfiliado = new FormData();
    this.storage.get("user_id").then((val)=>{this.id = val}) ;
    esAfiliado.append('id_usuario',this.id);
    esAfiliado.append('project_code',codigoProyecto);
    
    return this.http.post("http://"+this.urlIP+"/app/projects/isAffiliate",esAfiliado).map(res => res.json());
  }

  estaRegistrado(tipoLogin,userData){
    let usuario = new FormData();
    usuario.append('tipo_login',tipoLogin);
    if(tipoLogin === "F") usuario.append('facebook_id',userData.id);
    if(tipoLogin === "T") usuario.append('client_phone',userData.client_phone);
    return this.http.post("http://"+this.urlIP+"/app/validate/customer",usuario).map(res => res.json());
    
    //return this.http.post(this.facebookAPI,usuario).map(res => res.json());
  }

  obtenerToken(number){
    let telefono = new FormData();
    telefono.append('number',number);
    return this.http.post("http://"+this.urlIP+"/app/phoneCode",telefono).map(res => res.json());

  }

  datosCliente(userId:string){
    let cliente = new FormData();
    cliente.append('id_usuario',userId);
    return this.http.post("http://"+this.urlIP+"/app/datosCliente",cliente).map(res => res.json());
  }

  searchProducts(keyWord){
    let searchProduct = new FormData();
    searchProduct.append('keyWord',keyWord);
    return this.http.post("http://"+this.urlIP+"/app/searchProduct",searchProduct).map(res => res.json());
  }

  actualizarImagen(imagen,userId){
    let cliente = new FormData();
    cliente.append('user_image',imagen);
    cliente.append('id_usuario',userId);
    return this.http.post("http://"+this.urlIP+"/app/modificar/imagen",cliente).map(res => res.json());
  }

  actualizarNombre(nombre,userId){
    let cliente = new FormData();
    cliente.append('user_name',nombre);
    cliente.append('id_usuario',userId);
    return this.http.post("http://"+this.urlIP+"/app/modificar/nombre",cliente).map(res => res.json());
  }

  actualizarApellido(apellido,userId){
    let cliente = new FormData();
    cliente.append('user_surname',apellido);
    cliente.append('id_usuario',userId);
    return this.http.post("http://"+this.urlIP+"/app/modificar/apellido",cliente).map(res => res.json());
  }

  actualizarTelefono(telefono,userId){
    let cliente = new FormData();
    cliente.append('user_phone',telefono);
    cliente.append('id_usuario',userId);
    return this.http.post("http://"+this.urlIP+"/app/modificar/telefono",cliente).map(res => res.json());
  }

  actualizarCorreo(correo,userId){
    let cliente = new FormData();
    cliente.append('user_email',correo);
    cliente.append('id_usuario',userId);
    return this.http.post("http://"+this.urlIP+"/app/modificar/correo",cliente).map(res => res.json());
  }


  getUserId(){
    return this.storage.get("user_id");
  }

  cancelarCuenta(userId){
    let cliente = new FormData();
    cliente.append('client_id',userId);
    cliente.append('client_status','B');
    return this.http.post("http://"+this.urlIP+"/app/cancel/account",cliente).map(res => res.json());
  }

  actualizarDevice(userId,device){
    let cliente = new FormData();
    cliente.append('client_id',userId);
    cliente.append('client_device',device);
    return this.http.post("http://"+this.urlIP+"/app/update/device",cliente).map(res => res.json());
  }


}
