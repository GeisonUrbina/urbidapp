import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { DecisionPage } from '../pages/decision/decision';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = DecisionPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,storage: Storage) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

       //OneSignal Code start:
      // Enable to debug issues:
       //window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

      window["plugins"].OneSignal
        .startInit("531998ec-85dc-4b20-bf47-df8de55a51c6", "358103162694")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

        window["plugins"].OneSignal.addSubscriptionObserver(function (state) { 
          if (!state.from.subscribed && state.to.subscribed) {
            storage.set("user_device",state.to.userId);
          }  
        });
    });
  }

  
}

